@echo off
cls
set PATH=%PATH%;C:\Git\bin
set APP="TestWebApplication"
set DIST_PATH="D:\Repos\TestWebApplication\"
set FULL_PATH="D:\Repos\TestWebApplication\TestWebApplication"

IF EXIST %DIST_PATH% (
    sc stop %APP%
) ELSE (
    mkdir %DIST_PATH%
    sc create %APP% binPath=%FULL_PATH%
)

dotnet publish -c release -o %DIST_PATH%
sc start %APP%